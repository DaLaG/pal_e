package pal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Main
{
    static int N, M;
    static int[] reprez, rank;
    static int[][] graph, graphFreq;
    static List<Integer> spanTree;
    static Map<Integer, Integer> sortedMap;


    public static void main(String[] args)
    {
        /*long start = System.nanoTime();*/

        sortedMap = getGraphFromInput2();
        sortGraph(graph);
        graphFreq = sortGraph1(sortedMap, graph.clone());
        //System.out.println(sortedMap.toString());
        long price = doProcedure();
        //printMinimumSpanningTree();
        //System.out.println("Price of minimum spanning tree " + price);
        System.out.println(price);
        /*long stop = System.nanoTime();
        double time = ((double) (stop - start)) / 1000000000;
        System.out.println("Nacito zo suboru za: " + time);*/
    }


    public static Map<Integer, Integer> getGraphFromInput2()
    {

        Map<Integer, Integer> priceMap = new HashMap<Integer, Integer>();
        try
        {
            BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

            N = readInt(bf);
            M = readInt(bf);

            graph = new int[M][5];

            for (int i = 0; i < M; i++)
            {
                graph[i][0] = readInt(bf) - 1;
                graph[i][1] = readInt(bf) - 1;
                int p = readInt(bf);
                if (priceMap.containsKey(p))
                {
                    int value = priceMap.get(p) + 1;
                    priceMap.put(p, value);
                }
                else
                {
                    priceMap.put(p, 1);
                }
                graph[i][2] = p;

                //System.out.printf("%d %d %d\n", graph[i][0], graph[i][1], graph[i][2]);
            }

            bf.close();

        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }

        //System.out.println(priceMap.toString());
        return sortMap(priceMap);
        //return priceMap;
    }


    public static int readInt(BufferedReader in)
    {
        try
        {
            int ret = 0;
            boolean dig = false;
            for (int c = 0; (c = in.read()) != -1;)
            {
                if (c >= '0' && c <= '9')
                {
                    dig = true;
                    ret = ret * 10 + c - '0';
                }
                else if (dig)
                    break;
            }

            return ret;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return 0;
    }


    private static Map<Integer, Integer> sortMap(Map<Integer, Integer> unsortedMap)
    {
        Map<Integer, Integer> sortedMap = unsortedMap.entrySet().stream()
                .sorted(Entry.comparingByValue(new Comparator<Integer>()
                {

                    @Override
                    public int compare(Integer e1, Integer e2)
                    {
                        return e2 - e1;
                    }

                }))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue,
                        (e1, e2) -> e2, LinkedHashMap::new));
        return sortedMap;
    }


    private static void sortGraph(int[][] graph)
    {
        Arrays.sort(graph, new Comparator<int[]>()
        {
            @Override
            public int compare(int[] e1, int[] e2)
            {
                return e1[2] - e2[2];
            }
        });
    }


    private static int[][] sortGraph1(Map<Integer, Integer> sortedMap, int[][] graph)
    {
        for (int i = 0; i < M; i++)
        {
            graph[i][3] = sortedMap.get(graph[i][2]);

            graph[i][4] = i;
        }

        Arrays.sort(graph, new Comparator<int[]>()
        {
            @Override
            public int compare(int[] e1, int[] e2)
            {
                int p = e2[3] - e1[3];
                if (p == 0)
                    p = e1[2] - e2[2];
                return p;
            }
        });
        return graph;
    }


    public static long doProcedure()
    {

        long price = Integer.MAX_VALUE;
        int maxF = 0;
        int index1 = 0;
        int upToF = 0;
        do
        {
            int fr = graphFreq[index1][3];
            index1 += fr;
            if (fr < maxF)
                break;
            UF_init();
            upToF = 0;
            spanTree = new ArrayList<Integer>();
            long localPrice = 0;
            for (int i = index1 - fr; i < index1; i++)
            {
                if (UF_find(graphFreq[i][0]) != UF_find(graphFreq[i][1]))
                {

                    UF_union(reprez[graphFreq[i][0]], reprez[graphFreq[i][1]]);
                    localPrice += graphFreq[i][2];
                    spanTree.add(graphFreq[i][4]);
                    upToF++;
                    /*System.out.println(graph[i][0] + " -> " + graph[i][1] + " for " + graph[i][2]);*/
                }
            }
            localPrice = doKor(localPrice, upToF);
            if (maxF < upToF)
            {
                price = localPrice;
                maxF = upToF;
            }
            else if (maxF == upToF && price > localPrice)
            {
                price = localPrice;
            }
        }
        while (maxF <= upToF);

        return price;
    }


    private static long doKor(long price, int fr)
    {
        for (int i = 0; fr < N - 1; i++)
        {
            if (UF_find(graph[i][0]) != UF_find(graph[i][1]))
            {

                UF_union(reprez[graph[i][0]], reprez[graph[i][1]]);
                price += graph[i][2];
                spanTree.add(i);
                fr++;
                /*System.out.println(graph[i][0] + " -> " + graph[i][1] + " for " + graph[i][2]);*/
            }
        }
        /*System.out.println("reprez = " + Arrays.toString(reprez));
        System.out.println("rank = " + Arrays.toString(rank));*/
        return price;
    }


    private static void UF_init()
    {

        reprez = new int[N];
        rank = new int[N];
        for (int i = 0; i < N; i++)
        {
            reprez[i] = i;
            rank[i] = 0;
        }
    }


    private static void UF_union(int a, int b)
    {
        if (rank[b] > rank[a])
            reprez[a] = b;
        else
        {
            reprez[b] = a;
            if (rank[a] == rank[b])
                rank[a]++;
        }
    }


    private static int UF_find(int a)
    {
        int parent = reprez[a];
        if (parent != a)
            reprez[a] = UF_find(parent);
        return reprez[a];
    }


    public static void printMinimumSpanningTree()
    {
        String s = "";
        for (int i : spanTree)
        {
            s += String.format("%d -> %d za %d\n", graph[i][0] + 1, graph[i][1] + 1, graph[i][2]);
        }
        System.out.println(s);
    }
}
